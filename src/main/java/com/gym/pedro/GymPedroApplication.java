package com.gym.pedro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "edu.cibertec")
public class GymPedroApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymPedroApplication.class, args);	
	}

}
