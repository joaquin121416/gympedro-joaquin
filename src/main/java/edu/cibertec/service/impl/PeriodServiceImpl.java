package edu.cibertec.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.cibertec.dao.PeriodDAO;
import edu.cibertec.dao.RolDAO;
import edu.cibertec.dto.PeriodDTO;
import edu.cibertec.dto.RolDTO;
import edu.cibertec.service.PeriodService;
import edu.cibertec.service.RolService;

@Service(value = "PeriodService")
public class PeriodServiceImpl implements PeriodService{

	@Autowired
	PeriodDAO periodDAO;

	@Override
	public PeriodDTO insertarPeriod(PeriodDTO periodDTO) {
		// TODO Auto-generated method stub
		return periodDAO.insertarPeriod(periodDTO);
	}

	@Override
	public PeriodDTO actualizarPeriod(PeriodDTO periodDTO) {
		// TODO Auto-generated method stub
		return periodDAO.actualizarPeriod(periodDTO);
	}


}