package edu.cibertec.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.cibertec.dao.UsuarioDAO;
import edu.cibertec.dto.LoginDTO;
import edu.cibertec.dto.UsuariosDTO;
import edu.cibertec.service.UserService;

@Service(value = "UserService")
public class UserServiceImpl implements UserService{

	@Autowired
	UsuarioDAO usuarioDAO;

	@Override
	public List<UsuariosDTO> obtenerUsuarios() {
		// TODO Auto-generated method stub
		return usuarioDAO.obtenerUsuarios();
	}

	@Override
	public LoginDTO validarLogin(LoginDTO l) {
		return usuarioDAO.validarLogin(l);
	}
	
	@Override
	public UsuariosDTO obtenerUsuarioById(int id) {
		// TODO Auto-generated method stub
		return usuarioDAO.obtenerUsuariosById(id);
	}
	
}