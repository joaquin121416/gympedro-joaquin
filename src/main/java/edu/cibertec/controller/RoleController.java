package edu.cibertec.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import edu.cibertec.dto.RolDTO;
import edu.cibertec.service.RolService;


@RestController
@RequestMapping("/api/v1")
public class RoleController {

	private Logger logger = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	RolService rolService;
	
    @GetMapping(value = "/rol", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RolDTO> getRoles() {
  
        return rolService.obtenerRoles();
    }
    
    @GetMapping(value = "/rolById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RolDTO getRolById(@PathVariable("id") int id) {
  
        return rolService.obtenerRolesById(id);
    }
    
    @PostMapping(value = "/rol", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE )
    public RolDTO insertarRoles(@RequestBody RolDTO rol) {
    	
    	
    	logger.info("Info => " + rol);
        return rolService.insertarRol(rol);
    }
}
