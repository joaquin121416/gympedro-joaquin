package edu.cibertec.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.cibertec.dto.LoginDTO;
import edu.cibertec.dto.UsuariosDTO;
import edu.cibertec.service.UserService;

@RestController
@RequestMapping("/api/v1")
public class UserController {
	
	private Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
 
    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuariosDTO> getUsuarios() {
  
        return userService.obtenerUsuarios();
    }
    
    @Validated
    @PostMapping(value = "/validate", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public LoginDTO validarLogin(@RequestBody LoginDTO l) {
    	
    	logger.info("Info => " + l);
    		
        return userService.validarLogin(l);
        
    }
    
    
    @GetMapping(value = "/userById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuariosDTO getUsuarioById(@PathVariable("id") int id) {
  
        return userService.obtenerUsuarioById(id);
    }
    
}
