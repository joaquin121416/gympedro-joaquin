package edu.cibertec.dao;

import java.util.List;

import edu.cibertec.dto.PeriodDTO;
import edu.cibertec.dto.RolDTO;


public interface PeriodDAO {

	PeriodDTO insertarPeriod(PeriodDTO periodDTO);

	PeriodDTO actualizarPeriod(PeriodDTO periodDTO);
	

}
