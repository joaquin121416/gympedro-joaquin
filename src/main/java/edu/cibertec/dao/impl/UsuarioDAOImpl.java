package edu.cibertec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.cibertec.bd.util.UtilBD;
import edu.cibertec.dao.UsuarioDAO;
import edu.cibertec.dto.LoginDTO;
import edu.cibertec.dto.UsuariosDTO;

@Repository(value = "UsuarioDAO")
public class UsuarioDAOImpl implements UsuarioDAO {

	@Override
	public List<UsuariosDTO> obtenerUsuarios() {
		List<UsuariosDTO> lstUsuario = null;
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cnx = UtilBD.getConnection();
			String sql = "select * from Users";
			pst = cnx.prepareStatement(sql);

			rs = pst.executeQuery();

			lstUsuario = new ArrayList<UsuariosDTO>();

			while (rs.next()) {
				UsuariosDTO usuario = new UsuariosDTO();

				usuario.setUserId(rs.getInt(1));
				usuario.setUserName(rs.getString(2));
				usuario.setFullName(rs.getString(3));
				usuario.setEmailId(rs.getString(4));
				usuario.setContactno(rs.getString(5));
				usuario.setPassword(rs.getString(6));
				usuario.setCreatedby(rs.getInt(7));
				usuario.setCreatedDate(rs.getDate(8));
				usuario.setStatus(rs.getInt(9));

				lstUsuario.add(usuario);
			}
		} catch (Exception e) {
			System.out.println("Error en llenacombo :" + e.getMessage());
		}

		return lstUsuario;
	}

	@Override
	public LoginDTO validarLogin(LoginDTO l) {

		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "select UserName, Password, RoleId  from users as u \r\n" + 
					"inner join UsersInRoles as r\r\n" + 
					"on u.UserId = r.UserId where UserName = ? and Password = ?";
			
			pst = cnx.prepareStatement(sql);
			pst.setString(1, l.getUserName());
			pst.setString(2, l.getPassword());
			
			
			rs = pst.executeQuery();
			if(rs.next()) {
				l =new LoginDTO();
				
				l.setUserName(rs.getString(1));
				l.setPassword(rs.getString(2));
				l.setRoleId(rs.getInt(3));
			}
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return l;
	}
	
	
	@Override
	public UsuariosDTO obtenerUsuariosById(int id) {

		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		UsuariosDTO usuario = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "select * from Users where UserId = ?";
			
			pst = cnx.prepareStatement(sql);
			pst.setInt(1, id);
			
			
			rs = pst.executeQuery();
			if(rs.next()) {
				usuario =new UsuariosDTO();
				
				usuario.setUserId(rs.getInt(1));
				usuario.setUserName(rs.getString(2));
				usuario.setFullName(rs.getString(3));
				usuario.setEmailId(rs.getString(4));
				usuario.setContactno(rs.getString(5));
				usuario.setPassword(rs.getString(6));
				usuario.setCreatedby(rs.getInt(7));
				usuario.setCreatedDate(rs.getDate(8));
				usuario.setStatus(rs.getInt(9));
			}
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return usuario;
	}
}
