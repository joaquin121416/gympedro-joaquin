package edu.cibertec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.cibertec.bd.util.UtilBD;
import edu.cibertec.dao.PeriodDAO;
import edu.cibertec.dao.RolDAO;
import edu.cibertec.dto.PeriodDTO;
import edu.cibertec.dto.RolDTO;

@Repository(value = "PeriodDAO")
public class PeriodDAOImpl implements PeriodDAO {


	@Override
	public PeriodDTO actualizarPeriod(PeriodDTO periodDTO) {

		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "UPDATE PeriodTB set Text = ? , Value = ? where PeriodID = ?";
			
			pst = cnx.prepareStatement(sql);
			pst.setString(1, periodDTO.getText());
			pst.setString(2, periodDTO.getValue());
			pst.setInt(3, periodDTO.getPeriodID());
			
			pst.execute();
					
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return periodDTO;
	}

	@Override
	public PeriodDTO insertarPeriod(PeriodDTO periodDTO) {

		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = UtilBD.getConnection();
			String sql = "insert into  PeriodTB (Text,Value) values(?,?);";
			
			pst = cnx.prepareStatement(sql);
			pst.setString(1, periodDTO.getText());
			pst.setString(2, periodDTO.getValue());
			
			
			pst.execute();
		
			
			
		} catch (Exception e) {
			System.out.println("Error en la sentencia" + e.getMessage());
		} 

		return periodDTO;
	}

}
