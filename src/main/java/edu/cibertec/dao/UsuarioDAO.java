package edu.cibertec.dao;

import java.util.List;

import edu.cibertec.dto.LoginDTO;
import edu.cibertec.dto.UsuariosDTO;


public interface UsuarioDAO {
	
	public List<UsuariosDTO> obtenerUsuarios();
	
	public LoginDTO validarLogin(LoginDTO l);

	public UsuariosDTO obtenerUsuariosById(int id);
	
}
