package edu.cibertec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDetailsDTO implements Serializable {

	private Long PaymentID;
	private int PlanID;
	private int WorkouttypeID;
	private String Paymenttype;
	private Date PaymentFromdt;
	private Date PaymentTodt;
	private double PaymentAmount;
	private Date NextRenwalDate;
	private Date CreateDate;
	private int Createdby;
	private Date ModifyDate;
	private int ModifiedBy;
	private String RecStatus;
	private Long MemberID;
	private String MemberNo;

	public PaymentDetailsDTO(Long paymentID, int planID, int workouttypeID, String paymenttype, Date paymentFromdt,
			Date paymentTodt, double paymentAmount, Date nextRenwalDate, Date createDate, int createdby,
			Date modifyDate, int modifiedBy, String recStatus, Long memberID, String memberNo) {
		super();
		PaymentID = paymentID;
		PlanID = planID;
		WorkouttypeID = workouttypeID;
		Paymenttype = paymenttype;
		PaymentFromdt = paymentFromdt;
		PaymentTodt = paymentTodt;
		PaymentAmount = paymentAmount;
		NextRenwalDate = nextRenwalDate;
		CreateDate = createDate;
		Createdby = createdby;
		ModifyDate = modifyDate;
		ModifiedBy = modifiedBy;
		RecStatus = recStatus;
		MemberID = memberID;
		MemberNo = memberNo;
	}

	public PaymentDetailsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
