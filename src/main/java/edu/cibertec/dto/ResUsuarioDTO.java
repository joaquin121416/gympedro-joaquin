package edu.cibertec.dto;

import java.io.Serializable;
import java.util.List;

public class ResUsuarioDTO implements Serializable{

	
	private List<UsuariosDTO> users;

	public List<UsuariosDTO> getUsers() {
		return users;
	}

	public void setUsers(List<UsuariosDTO> users) {
		this.users = users;
	}
	
	
	
}
